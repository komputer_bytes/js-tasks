$(document).ready(function() {
	$("#addBox BUTTON").click(addTask);
	$(".add").click(showAdd);
	$(".delete").click(delItems);
	if(typeof(Storage)!=="undefined") {
		$("TABLE TBODY").append(localStorage.tasks);
	}
	if($('TABLE TBODY').children().length > 0) {
			$('TABLE').show();
			$('#info').hide();
			updateTable();
		}
});
/**
function to add a task to our list
@param e event the event 
**/
function addTask(e) {
	e.preventDefault();
	var task = $("#task").val();
	var priority = $("#priority").val();
	//break out the tags into an array
	var tags = $("#tags").val().split(','); //array
	var tagout = '';
	var trItems = $("#tags").val();
	for(var a = 0;a < tags.length;a++) {
		tagout += '<span>' + tags[a] + '</span>';
	}
	//add the new task to the table, and some extra data to work with
	$("TABLE TBODY").append("<tr class='" + priority + "' data-tags='" + trItems + "'><td>" + task + "</td><td>" + priority + "</td><td>" + tagout + "</td></tr>");
	//clear the add task values
	$('#task').val("");
	$("#tags").val("");
	//hide the add task box
	$('#addBox').removeClass("show");
	$('#info').hide();
	//run some table updates
	updateTable();
	updateStorage()
}
/**
function to show the add task dialog
@param e event the event 
**/
function showAdd(e) {
	e.preventDefault();
	//show the box
	$('#addBox').addClass("show");
}
/**
function to highlight boxes that are selected, or unhighlight them if they are already 
@param e event the event 
**/
function highLightBox(e) {
	//use data-selected to determine which boxes the user wants to interact with
	if($(this).attr("data-selected") != "selected") {
		//if the box isn't highlighted add the highlight
		$(this).attr("data-selected","selected");
		$(this).addClass("selected");
	}
	else {
		//remove it otherwise
		$(this).attr("data-selected","");
		$(this).removeClass("selected");
	}
}
/**
delete the items the user has highlighted
@param e event the event 
**/
function delItems(e) {
	if(confirm("Are you sure you want to delete the selected tasks?")) {
		$("[data-selected='selected']").remove();
		if($('TABLE TBODY').children().length < 1) {
			$('TABLE').hide();
			$('#info').show();
			updateStorage()
		}
	}
}
/**
update table adds highlight to seperate cells, and adds the click event handler
**/
function updateTable() {
	$( "TABLE TBODY TR" ).each(function(index) {
		//mod 2 evaluates to 1 on an odd number and 0 on an even so it is great to select every other row
		if((index%2)) {
			$( this ).addClass("dark");
		}
	});
	$("TABLE TBODY TR").click(highLightBox);
	$("TABLE").show();
}

function updateStorage() {
	if(typeof(Storage)!=="undefined") {
		// Code for localStorage/sessionStorage.
		localStorage.tasks =  $("TABLE TBODY").html();
	  }
}